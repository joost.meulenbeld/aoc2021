# AOC2021
Advent of Code 2021

Note the CLI which can download input files directly.
## Usage
This project uses the [python poetry package manager](https://python-poetry.org/) to manage dependencies and virtual
environment. After setting this up, install the virtual environment using:
```shell
poetry install
```

After installing the environment, find out what the CLI can do by running
```shell
poetry run aoc2021 --help
```

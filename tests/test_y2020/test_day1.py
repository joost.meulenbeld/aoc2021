import pytest

from aoc2021.days.y2020.day1 import part1, part2


@pytest.mark.parametrize(("input", "expected"), [])
def test_part1(input: str, expected):
    result = part1(input)

    assert result == expected


@pytest.mark.parametrize(("input", "expected"), [])
def test_part2(input: str, expected):
    result = part2(input)

    assert result == expected

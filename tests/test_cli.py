"""Test the CLI functions."""

from click.testing import CliRunner

from aoc2021.__main__ import main


def test_cli_main():
    """Test that the main command does what it's supposed to."""
    runner = CliRunner()

    result = runner.invoke(main)

    assert result.exit_code == 0

import itertools
from pathlib import Path

import numpy as np


def parse_input(input: str):
    stripped = input.strip()
    parsed = stripped.splitlines()
    parsed = [list(map(int, line)) for line in parsed]
    return np.array(parsed, dtype=int)


def part1(input):
    parsed = parse_input(input)
    padded = np.pad(parsed, 1, mode="constant", constant_values=9)
    score = 0
    for i, j in itertools.product(range(1, padded.shape[0] - 1), range(1, padded.shape[1] - 1)):
        v = padded[i, j]
        if v < padded[i - 1, j] and v < padded[i + 1, j] and v < padded[i, j - 1] and v < padded[i, j + 1]:
            score += v + 1

    return score


def part2(input):
    parsed = parse_input(input)
    in_basin = parsed != 9
    basin_sizes = list()
    already_checked = set()
    for i, j in itertools.product(range(parsed.shape[0]), range(parsed.shape[1])):
        if not in_basin[i, j] or (i, j) in already_checked:
            continue

        this_basin_indices = set()
        this_basin_to_check = {(i, j)}
        while this_basin_to_check:
            i, j = this_basin_to_check.pop()
            this_basin_indices.add((i, j))

            for i_n, j_n in [(i + 1, j), (i - 1, j), (i, j + 1), (i, j - 1)]:
                if (
                    0 <= i_n < parsed.shape[0]
                    and 0 <= j_n < parsed.shape[1]
                    and in_basin[i_n, j_n]
                    and not (i_n, j_n) in this_basin_indices
                    and not (i_n, j_n) in this_basin_to_check
                ):
                    this_basin_to_check.add((i_n, j_n))
        already_checked.update(this_basin_indices)
        basin_sizes.append(len(this_basin_indices))
    sorted_basin_sizes = sorted(basin_sizes, reverse=True)
    return sorted_basin_sizes[0] * sorted_basin_sizes[1] * sorted_basin_sizes[2]


if __name__ == "__main__":
    year = 2021
    day = 9
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print(
        "Test: ",
        part2(
            """2199943210
3987894921
9856789892
8767896789
9899965678"""
        ),
    )
    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

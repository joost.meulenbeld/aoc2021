from pathlib import Path


def parse_input(input: str):
    stripped = input.strip()
    return list(map(int, stripped.split(",")))


def part1(input):
    parsed = parse_input(input)
    best_score = len(parsed) * max(parsed)
    for base in range(max(parsed)):
        score = sum(abs(base - number) for number in parsed)
        if score < best_score:
            best_score = score
    return best_score


def part2(input):
    parsed = parse_input(input)
    best_score = len(parsed) * max(parsed) ** 2
    for base in range(max(parsed)):
        distances = (abs(base - number) for number in parsed)
        score = sum(int(distance * (distance + 1) / 2) for distance in distances)
        if score < best_score:
            best_score = score
    return best_score


if __name__ == "__main__":
    year = 2021
    day = 7
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    test = """16,1,2,0,4,2,7,1,2,14"""
    print("Test:", part2(test))
    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

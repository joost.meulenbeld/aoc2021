from pathlib import Path
import itertools

import numpy as np


def parse_input(input: str):
    stripped = input.strip()
    parsed = stripped.splitlines()
    parsed = [list(map(int, line)) for line in parsed]
    return np.array(parsed, dtype=int)


class Octopus:
    def __init__(self, energy: int):
        self.energy = energy
        self.flashed_this_round = False
        self.neighbors = list()
        self.flashed_n_times = 0

    def increment(self):
        self.energy += 1
        if self.energy > 9 and not self.flashed_this_round:
            self.flashed_this_round = True
            for neighbor in self.neighbors:
                neighbor.increment()

    def reset(self):
        if self.flashed_this_round:
            self.flashed_n_times += 1
            self.energy = 0
        self.flashed_this_round = False

    def __repr__(self):
        return str(self.energy)
        return f"o<{self.energy}>"


def print_grid(octopi_grid):
    print("[")
    for line in octopi_grid:
        print(line)
    print("]")


def part1(input):
    parsed = parse_input(input)
    octopi_grid = list()
    for row in parsed:
        octopi_grid.append([Octopus(i) for i in row])
    octopi = [o for row in octopi_grid for o in row]

    for i, j in itertools.product(range(parsed.shape[0]), range(parsed.shape[1])):
        for di, dj in itertools.product(range(-1, 2), range(-1, 2)):
            ni = i + di
            nj = j + dj
            if (ni, nj) != (i, j) and 0 <= ni < parsed.shape[0] and 0 <= nj < parsed.shape[1]:
                octopi_grid[i][j].neighbors.append(octopi_grid[ni][nj])
    for i in range(100):
        for o in octopi:
            o.increment()
        for o in octopi:
            o.reset()
        # print(f"After {i}")
        # print_grid(octopi_grid)
    return sum(o.flashed_n_times for o in octopi)


def part2(input):
    parsed = parse_input(input)
    octopi_grid = list()
    for row in parsed:
        octopi_grid.append([Octopus(i) for i in row])
    octopi = [o for row in octopi_grid for o in row]

    for i, j in itertools.product(range(parsed.shape[0]), range(parsed.shape[1])):
        for di, dj in itertools.product(range(-1, 2), range(-1, 2)):
            ni = i + di
            nj = j + dj
            if (ni, nj) != (i, j) and 0 <= ni < parsed.shape[0] and 0 <= nj < parsed.shape[1]:
                octopi_grid[i][j].neighbors.append(octopi_grid[ni][nj])
    for i in range(1, 100000000):
        for o in octopi:
            o.increment()
        if all(o.flashed_this_round for o in octopi):
            return i
        for o in octopi:
            o.reset()


if __name__ == "__main__":
    year = 2021
    day = 11
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print(
        "Test:",
        part2(
            """
5483143223
2745854711
5264556173
6141336146
6357385478
4167524645
2176841721
6882881134
4846848554
5283751526
    """.strip()
        ),
    )
    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

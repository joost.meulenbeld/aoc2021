from pathlib import Path
from typing import Tuple
import numpy as np
from dataclasses import dataclass


@dataclass
class Coordinate:
    x: int
    y: int


@dataclass
class Line:
    c1: Coordinate
    c2: Coordinate

    @property
    def is_horizontal(self) -> bool:
        return self.c1.y == self.c2.y

    @property
    def is_vertical(self) -> bool:
        return self.c1.x == self.c2.x

    def get_indexer(self) -> Tuple[slice, slice]:
        x_dir = 1 if self.c2.x > self.c1.x else -1
        y_dir = 1 if self.c2.y > self.c1.y else -1
        return (list(range(self.c1.y, self.c2.y + y_dir, y_dir)), list(range(self.c1.x, self.c2.x + x_dir, x_dir)))


def line_from_text(text: str) -> Line:
    left, right = text.split(" -> ")
    return Line(
        c1=Coordinate(*map(int, left.split(","))),
        c2=Coordinate(*map(int, right.split(","))),
    )


def parse_input(input: str):
    stripped = input.strip()
    parsed = stripped.splitlines()
    return list(map(line_from_text, parsed))


def part1(input):
    lines = parse_input(input)
    lines = [line for line in lines if line.is_horizontal or line.is_vertical]
    max_x = max(max(l.c1.x, l.c2.x) for l in lines)
    max_y = max(max(l.c1.y, l.c2.y) for l in lines)
    grid = np.zeros((max_y + 1, max_x + 1), dtype=int)
    for line in lines:
        grid[line.get_indexer()] += 1
    return (grid > 1).sum()


def part2(input):
    lines = parse_input(input)
    max_x = max(max(l.c1.x, l.c2.x) for l in lines)
    max_y = max(max(l.c1.y, l.c2.y) for l in lines)
    grid = np.zeros((max_y + 1, max_x + 1), dtype=int)
    for line in lines:
        grid[line.get_indexer()] += 1
    return (grid > 1).sum()


if __name__ == "__main__":
    year = 2021
    day = 5
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    test = """
0,9 -> 5,9
8,0 -> 0,8
9,4 -> 3,4
2,2 -> 2,1
7,0 -> 7,4
6,4 -> 2,0
0,9 -> 2,9
3,4 -> 1,4
0,0 -> 8,8
5,5 -> 8,2
"""
    part2(test)
    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

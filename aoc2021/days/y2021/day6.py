from pathlib import Path
from collections import defaultdict


def parse_input(input: str):
    stripped = input.strip()
    return list(map(int, stripped.split(",")))


def solution(input, days):
    parsed = parse_input(input)
    lanterns = defaultdict(int)
    for l in parsed:
        lanterns[l] += 1
    for _ in range(days):
        new_ones = lanterns[0]
        for i in range(9):
            lanterns[i] = lanterns[i + 1]
        lanterns[6] += new_ones
        lanterns[8] = new_ones
    return sum(lanterns.values())


if __name__ == "__main__":
    year = 2021
    day = 6
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print("Test: ", solution("3,4,3,1,2", 80))
    print("Part 1:", solution(contents, 80))
    print("Part 2:", solution(contents, 256))

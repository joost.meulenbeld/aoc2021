from pathlib import Path
from queue import LifoQueue


def parse_input(input: str):
    stripped = input.strip()
    parsed = stripped.splitlines()
    return parsed


def part1(input):
    parsed = parse_input(input)
    opening = "([{<"
    closing = ")]}>"
    values = [3, 57, 1197, 25137]
    score = 0
    for line in parsed:
        stack = LifoQueue()
        for char in line:
            if char in opening:
                stack.put(closing[opening.index(char)])
            elif char != stack.get():
                score += values[closing.index(char)]
                break
    return score


def part2(input):
    parsed = parse_input(input)
    opening = "([{<"
    closing = ")]}>"
    values = [1, 2, 3, 4]
    scores = list()
    for line in parsed:
        stack = LifoQueue()
        for char in line:
            if char in opening:
                stack.put(closing[opening.index(char)])
            elif char != stack.get():
                stack = LifoQueue()
                break
        if stack.empty():
            continue
        score = 0
        while not stack.empty():
            score = score * 5 + values[closing.index(stack.get())]
        scores.append(score)
    scores.sort()
    return scores[len(scores) // 2]


if __name__ == "__main__":
    year = 2021
    day = 10
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print(
        "Test:",
        part2(
            """
[({(<(())[]>[[{[]{<()<>>
[(()[<>])]({[<{<<[]>>(
{([(<{}[<>[]}>{[]{[(<()>
(((({<>}<{<{<>}{[]{[]{}
[[<[([]))<([[{}[[()]]]
[{[{({}]{}}([{[{{{}}([]
{<[[]]>}<{[{[{[]{()[[[]
[<(<(<(<{}))><([]([]()
<{([([[(<>()){}]>(<<{{
<{([{{}}[<[[[<>{}]]]>[]]
""".strip()
        ),
    )
    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

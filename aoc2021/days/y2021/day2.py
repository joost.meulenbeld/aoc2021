from pathlib import Path


def parse_input(input: str):
    stripped = input.strip()
    parsed = stripped.splitlines()
    return parsed


def part1(input):
    parsed = parse_input(input)
    x, y = 0, 0
    for line in parsed:
        X = int(line.split(" ")[1])
        if line.startswith("forward"):
            x += X
        if line.startswith("down"):
            y += X
        if line.startswith("up"):
            y -= X
    return x * y


def part2(input):
    parsed = parse_input(input)
    horizontal, aim, depth = 0, 0, 0
    for line in parsed:
        X = int(line.split(" ")[1])
        if line.startswith("forward"):
            horizontal += X
            depth += aim * X
        if line.startswith("down"):
            aim += X
        if line.startswith("up"):
            aim -= X

    return horizontal * depth


if __name__ == "__main__":
    year = 2021
    day = 2
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

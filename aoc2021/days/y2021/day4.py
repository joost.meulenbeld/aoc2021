import numpy as np
from pathlib import Path
from dataclasses import dataclass


@dataclass
class Board:
    numbers: np.array
    bools: np.array


def is_finished(board: Board) -> bool:
    return board.bools.all(axis=0).any() or board.bools.all(axis=1).any()


def get_score(board: Board) -> int:
    return board.numbers[~board.bools].sum()


def draw_number(board: Board, number: int):
    board.bools[board.numbers == number] = True


def parse_input(input: str):
    stripped = input.strip()
    parsed = stripped.splitlines()
    numbers = list(map(int, parsed[0].split(",")))
    boards = list()
    for i in range(2, len(parsed), 6):
        board_text = parsed[i : i + 5]
        board_integers = [list(map(int, line.split())) for line in board_text]
        boards.append(Board(np.array(board_integers), np.zeros((5, 5), bool)))
    return numbers, boards


def part1(input):
    numbers, boards = parse_input(input)
    for number in numbers:
        for board in boards:
            draw_number(board, number)
            if is_finished(board):
                return number * get_score(board)


def part2(input):
    numbers, boards = parse_input(input)
    unfinished_boards = set(range(len(boards)))
    for number in numbers:
        for i_board, board in enumerate(boards):
            if i_board not in unfinished_boards:
                continue
            draw_number(board, number)
            if is_finished(board):
                unfinished_boards.remove(i_board)
                if not unfinished_boards:
                    return number * get_score(board)


if __name__ == "__main__":
    year = 2021
    day = 4
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

from pathlib import Path

import numpy as np


def numpy_binary_to_decimal(arr):
    return sum(n * 2 ** i for i, n in enumerate(reversed(arr)))


def parse_input(input: str):
    stripped = input.strip()
    parsed = stripped.splitlines()
    parsed = [list(map(int, list(row))) for row in parsed]
    parsed = np.array(parsed, dtype=int)
    return parsed


def part1(input):
    parsed = parse_input(input)
    most_common = parsed.mean(axis=0).round().astype(int)
    least_common = np.ones_like(most_common) - most_common

    most_common_decimal = numpy_binary_to_decimal(most_common)
    least_common_decimal = numpy_binary_to_decimal(least_common)

    return most_common_decimal * least_common_decimal


def part2(input):
    parsed = parse_input(input)
    numbers = parsed
    for col in range(parsed.shape[1]):
        number_of_ones = numbers[:, col].sum()
        if number_of_ones == numbers.shape[0] / 2:
            selector = 1
        else:
            selector = int(number_of_ones > numbers.shape[0] / 2)
        numbers = numbers[numbers[:, col] == selector, :]
        if numbers.shape[0] == 1:
            oxygen_rating = numpy_binary_to_decimal(numbers[0])

    numbers = parsed
    for col in range(parsed.shape[1]):
        number_of_ones = numbers[:, col].sum()
        if number_of_ones == numbers.shape[0] / 2:
            selector = 0
        else:
            selector = 1 - int(number_of_ones > numbers.shape[0] / 2)
        numbers = numbers[numbers[:, col] == selector, :]
        if numbers.shape[0] == 1:
            co2_rating = numpy_binary_to_decimal(numbers[0])

    return oxygen_rating * co2_rating


if __name__ == "__main__":
    year = 2021
    day = 3
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print(
        "Test: ",
        part2(
            """
00100
11110
10110
10111
10101
01111
00111
11100
10000
11001
00010
01010
    """
        ),
    )
    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

from pathlib import Path
from collections import defaultdict
from queue import Queue


def parse_input(input: str):
    stripped = input.strip()
    parsed = stripped.splitlines()
    caves = defaultdict(list)
    for line in parsed:
        left, right = line.split("-")
        caves[left].append(right)
        caves[right].append(left)
    return caves


def part1(input):
    parsed = parse_input(input)
    finished_routes = list()
    routes = Queue()
    routes.put(["start"])
    while not routes.empty():
        route = routes.get()
        for next_cave in parsed[route[-1]]:
            if next_cave == "end":
                finished_routes.append(route)
            elif next_cave not in route or next_cave.upper() == next_cave:
                routes.put([*route, next_cave])
    return len(finished_routes)


def part2(input):
    parsed = parse_input(input)
    finished_routes = list()
    routes = Queue()
    routes.put(["start"])
    while not routes.empty():
        route = routes.get()
        for next_cave in parsed[route[-1]]:
            if next_cave == "end":
                finished_routes.append(route)
                continue
            if next_cave == "start":
                continue
            small_caves_in_route = [cave for cave in route if cave.lower() == cave]
            small_cave_double_in_root = len(small_caves_in_route) != len(set(small_caves_in_route))
            if next_cave not in route or next_cave.upper() == next_cave or not small_cave_double_in_root:
                routes.put([*route, next_cave])
    return len(finished_routes)


if __name__ == "__main__":
    year = 2021
    day = 12
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print(
        "Test",
        part2(
            """
start-A
start-b
A-c
A-b
b-d
A-end
b-end
    """.strip()
        ),
    )
    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

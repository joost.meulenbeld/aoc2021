from pathlib import Path


def parse_input(input: str):
    stripped = input.strip()
    parsed = stripped.splitlines()
    parsed = list(map(int, parsed))
    return parsed


def part1(input):
    parsed = parse_input(input)
    increases = (i2 > i1 for i1, i2 in zip(parsed, parsed[1:]))
    return sum(increases)


def part2(input):
    parsed = parse_input(input)
    sums = [sum(numbers) for numbers in zip(parsed, parsed[1:], parsed[2:])]
    return sum(i2 > i1 for i1, i2 in zip(sums, sums[1:]))


if __name__ == "__main__":
    year = 2021
    day = 1
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

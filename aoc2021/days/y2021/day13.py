import numpy as np
from pathlib import Path

from dataclasses import dataclass


@dataclass
class Fold:
    direction: str
    location: int


def parse_input(input: str):
    stripped = input.strip()
    coord_text, fold_text = stripped.split("\n\n")

    tuples = [tuple(map(int, line.split(","))) for line in coord_text.splitlines()]
    max_x = max(t[0] for t in tuples)
    max_y = max(t[1] for t in tuples)
    grid = np.zeros((max_y + 1, max_x + 1), dtype=bool)
    for x, y in tuples:
        grid[y, x] = True

    folds = list()
    for line in fold_text.splitlines():
        left, right = line[len("fold along ") :].split("=")
        folds.append(Fold(left, int(right)))

    return grid, folds


def part1(input):
    grid, folds = parse_input(input)
    for fold in folds:
        if fold.direction == "x":
            grid = grid[:, : fold.location] | grid[:, fold.location + 1 :][:, ::-1]
        elif fold.direction == "y":
            grid = grid[: fold.location, :] | grid[fold.location + 1 :, :][::-1, :]
        else:
            raise
        return grid.sum()


def part2(input):
    grid, folds = parse_input(input)
    for fold in folds:
        if fold.direction == "x":
            grid = grid[:, : fold.location] | grid[:, fold.location + 1 :][:, ::-1]
        elif fold.direction == "y":
            grid = grid[: fold.location, :] | grid[fold.location + 1 :, :][::-1, :]
        else:
            raise
    return "\n".join("".join("x" if item else " " for item in line) for line in grid.astype(int))


if __name__ == "__main__":
    year = 2021
    day = 13
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print(
        "Test",
        part1(
            """
6,10
0,14
9,10
0,3
10,4
4,11
6,0
6,12
4,1
0,13
10,12
3,4
3,0
8,4
1,10
2,14
8,10
9,0

fold along y=7
fold along x=5    """.strip()
        ),
    )
    print("Part 1:", part1(contents))
    print("Part 2:\n")
    print(part2(contents))

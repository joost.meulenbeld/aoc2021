from pathlib import Path


def parse_input(input: str):
    return input.strip()


def part1(input):
    parsed = parse_input(input)
    floor = 0
    for char in parsed:
        if char == "(":
            floor += 1
        if char == ")":
            floor -= 1
    return floor


def part2(input):
    parsed = parse_input(input)
    floor = 0
    for i, char in enumerate(parsed, start=1):
        if char == "(":
            floor += 1
        if char == ")":
            floor -= 1
        if floor < 0:
            return i
    return floor


if __name__ == "__main__":
    year = 2015
    day = 1
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

from pathlib import Path


def parse_input(input: str):
    lines = input.strip().splitlines()
    integers = [int(line) for line in lines]

    return integers


def part1(input):
    result = parse_input(input)
    for i in result:
        for j in result:
            if i + j == 2020:
                return i * j


def part2(input):
    result = parse_input(input)
    for i in result:
        for j in result:
            for k in result:
                if i + j + k == 2020:
                    return i * j * k


if __name__ == "__main__":
    year = 2020
    day = 1
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

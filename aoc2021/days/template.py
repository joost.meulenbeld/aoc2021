from pathlib import Path


def parse_input(input: str):
    stripped = input.strip()
    parsed = stripped.splitlines()
    return parsed


def part1(input):
    parsed = parse_input(input)
    print(parsed)


def part2(input):
    parsed = parse_input(input)
    print(parsed)


if __name__ == "__main__":
    year = 2018
    day = 2
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

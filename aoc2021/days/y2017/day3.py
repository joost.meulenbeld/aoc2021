from pathlib import Path
from itertools import cycle


def parse_input(input: str):
    stripped = input.strip()
    return int(stripped)


def part1(input):
    parsed = parse_input(input)
    directions = [(1, 0), (0, 1), (-1, 0), (0, -1)]
    dirs = cycle(directions)
    steps = (i for i in range(parsed) for _ in range(2))
    change_direction = (i == 0 for step in steps for i in range(step))

    loc = (0, 0)
    while True:
        # for _ in tqdm(range(parsed)):
        parsed -= 1
        if parsed == 0:
            return abs(loc[0]) + abs(loc[1])
        if next(change_direction):
            direction = next(dirs)
        loc = (loc[0] + direction[0], loc[1] + direction[1])


def part2(input):
    # parsed = parse_input(input)
    pass


if __name__ == "__main__":
    year = 2017
    day = 3
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

from pathlib import Path


def parse_input(input: str):
    stripped = input.strip()

    return stripped


def part1(input):
    parsed = parse_input(input)
    p = f"{parsed}{parsed[0]}"
    t = 0
    for i, j in zip(p, p[1:]):
        if i == j:
            t += int(i)
    return t


def part2(input):
    parsed = parse_input(input)
    p = f"{parsed}{parsed}"
    t = 0
    for i in range(len(parsed)):
        if p[i] == p[i + len(parsed) // 2]:
            t += int(p[i])
    return t


if __name__ == "__main__":
    year = 2017
    day = 1
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

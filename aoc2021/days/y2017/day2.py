from pathlib import Path


def parse_input(input: str):
    stripped = input.strip()
    lines = stripped.splitlines()
    rows = list()
    for line in lines:
        rows.append([int(i) for i in line.split("\t")])

    return rows


def part1(input):
    parsed = parse_input(input)
    t = 0
    for row in parsed:
        difference = max(row) - min(row)
        t += difference
    return t


def part2(input):
    parsed = parse_input(input)
    t = 0
    for row in parsed:
        for i, num1 in enumerate(row):
            for num2 in row[i + 1 :]:
                if num1 // num2 == num1 / num2:
                    t += num1 // num2
                elif num2 // num1 == num2 / num1:
                    t += num2 // num1
    return t


if __name__ == "__main__":
    year = 2017
    day = 2
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

from pathlib import Path

from collections import Counter


def parse_input(input: str):
    stripped = input.strip()
    lines = stripped.splitlines()
    return lines


def part1(input):
    parsed = parse_input(input)
    count2 = 0
    count3 = 0
    for line in parsed:
        c = Counter(line.strip())
        if 2 in set(c.values()):
            count2 += 1

        if 3 in set(c.values()):
            count3 += 1
    return count2 * count3


def part2(input):
    parsed = parse_input(input)
    for i, p1 in enumerate(parsed):
        for p2 in parsed[i + 1 :]:
            correct_letters = list()
            for l1, l2 in zip(p1, p2):
                if l1 == l2:
                    correct_letters.append(l1)
            if len(correct_letters) == len(p1) - 1:
                return "".join(correct_letters)


if __name__ == "__main__":
    year = 2018
    day = 2
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

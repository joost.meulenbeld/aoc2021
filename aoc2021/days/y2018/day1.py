from pathlib import Path
from itertools import cycle


def parse_input(input: str):
    stripped = input.strip()
    lines = stripped.splitlines()
    ints = list()
    for line in lines:
        if line[0] == "+":
            ints.append(int(line[1:]))
        else:
            ints.append(-int(line[1:]))
    return ints


def part1(input):
    parsed = parse_input(input)
    print(parsed)
    return sum(parsed)


def part2(input):
    parsed = parse_input(input)
    p = cycle(parsed)
    all_ints = set()
    current_number = 0
    while True:
        if current_number in all_ints:
            return current_number
        all_ints.add(current_number)
        current_number = current_number + next(p)


if __name__ == "__main__":
    year = 2018
    day = 1
    path = Path("input_files", str(year), f"{day}.txt")
    contents = path.read_text()

    print("Part 1:", part1(contents))
    print("Part 2:", part2(contents))

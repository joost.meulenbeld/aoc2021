"""Public command-line interface for AOC2021."""
import requests
from pathlib import Path

import click


@click.group()
def main():
    """Run main command of the program."""


@main.command()
@click.argument("year", type=int)
@click.argument("day", type=int)
@click.option("--session", "-s", type=str, help="Session cookie.")
@click.option(
    "--output-path-template",
    "-o",
    type=click.Path(writable=True),
    help="Template for folder to output result.",
    default=str(Path("input_files", "{year}", "{day}.txt")),
    show_default=True,
)
def download_input_data(year: int, day: int, session: str, output_path_template: str):
    """Download the input data file for a given day in a given aoc year."""
    url = f"https://adventofcode.com/{year}/day/{day}/input"
    output_path = output_path_template.format(year=year, day=day)
    Path(output_path).parent.mkdir(exist_ok=True, parents=True)

    response = requests.get(url, cookies={"session": session})

    if response.status_code == 200:
        print(f"Writing file to {output_path}")
        with open(output_path, "wb") as fp:
            fp.write(response.content)
    else:
        print(f"Failed with status code {response.status_code} and message\n\n{response.content.decode()}")


if __name__ == "__main__":
    main()

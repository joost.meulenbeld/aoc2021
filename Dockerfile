FROM python:3.8-slim-buster
# Change to 3.8-buster if you need more system libraries

RUN pip install --upgrade pip && \
    pip install poetry && \
    poetry -V

WORKDIR /app

COPY poetry.lock pyproject.toml README.md ./
COPY lib ./lib

# First poetry install to install the dependencies
RUN poetry install --no-root -v

COPY tests tests
COPY aoc2021 aoc2021

# Second poetry install installs the package itself and takes very little time.
RUN poetry install -v

# The entrypoint poetry run will run any command, and has the correct environment already activated.
ENTRYPOINT ["poetry", "run"]
